<?php
//引入类库
require '../SpringMySQLi.php';

$dbHost = '127.0.0.1';  // database host
$dbUser = 'root';       // database username
$dbUpwd = '';           // database password
$dbName = 'test';       // database name

/*
--
-- 测试表结构
--
CREATE TABLE IF NOT EXISTS `member` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL DEFAULT '',
  `login_pwd` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(300) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '1',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='会员表' AUTO_INCREMENT=7 ;

--
-- 插入测试表中数据
--
INSERT INTO `member` (`mid`, `username`, `login_pwd`, `email`, `status`, `update_time`, `delete_flag`) VALUES
(1, 'Zack', '96e79218965eb72c92a549dd5a330112', 'zack@test.com', 1, '2021-06-02 10:48:51', 0),
(2, 'Young', '96e79218965eb72c92a549dd5a330112', 'young@test.com', 1, '2021-06-02 10:48:51', 0),
(3, 'Wade', '96e79218965eb72c92a549dd5a330112', 'wade@test.com', 1, '2021-06-02 10:49:39', 0),
(4, 'Wallace', '96e79218965eb72c92a549dd5a330112', 'wallace@test.com', 1, '2021-06-02 10:49:39', 0),
(5, 'Venus', '96e79218965eb72c92a549dd5a330112', 'venus@test.com', 1, '2021-06-02 10:50:47', 0),
(6, 'Underwood', '96e79218965eb72c92a549dd5a330112', 'underwood@test.com', 0, '2021-06-02 10:50:47', 0);
*/

$dbh = new SpringMySQLi($dbHost, $dbUser, $dbUpwd, $dbName);

$sql = 'SELECT * FROM member WHERE mid=#1';
$mid = 3;
$arr = $dbh->selectRow($sql, $mid);
print_r($arr);

$sql = 'SELECT * FROM member WHERE email LIKE \'%#1%\'';
$kw  = 'test.com';
$arr = $dbh->select($sql, $kw);
print_r($arr);

$sql = 'SELECT * FROM member WHERE username LIKE \'#1%\' AND status=#2';
$kw  = 'W';
$arr = $dbh->select($sql, $kw, '1');
print_r($arr);

$arr = array(
    'username'  => 'Taylor',
    'login_pwd' => md5('111111'),
    'email'     => 'taylor@test.com',
    'status'    => 1,
);
$mid = $dbh->insert('member', $arr);
echo 'inserted new member: ', $mid;

echo PHP_EOL;
$arr = $dbh->getLogs();
print_r($arr);
