# SpringMySQLi

#### 类库介绍
封装 MySQLi 扩展实现的对 MySQL数据库 的常用便捷操作类库

#### 类库方法
1. public 参数
    + $pageNo;         分页查询时当前查询页码
    + $pageRows;       分布时每页获取的记录行数
    + $runCount;       SQL执行次数
    + $runTime;        SQL执行消耗时间(s)
    + $errNo;          错误代码
    + $errMsg;         错误描述信息
2. public 方法
    + __construct($host, $user, $pwd, $dbname, $charset = 'utf8mb4')  类库初始化（传入数据库连接所需参数）
    + __destruct()  类库自动销毁时执行代码
    + destory()  类库手工销毁（关闭释放数据库连接资源）
    + setPageNo($num)  设置分页查询时当前查询页码
    + setPageRows($num)  设置分布时每页获取的记录行数
    + setDbName($name)  设置默认操作的数据库名称
    + escape($val)  过滤指定参数（防注入）
    + select($sql, $var1, $var2, $var3...)  执行$sql语句并返回全部结果数组
    + selectPage($sql, $var1, $var2, $var3...)  执行$sql语句并返回分页结果数组
    + selectRow($sql, $var1, $var2, $var3...)  执行$sql语句并返回一行结果数组
    + selectOne($sql, $var1, $var2, $var3...)  执行$sql语句并返回第一行第一列字段值
    + selectHash($sql, $var1, $var2, $var3...)  执行$sql语句并返回结果集第一二列的hash形式数组
    + exe($sql)  执行$sql语句
    + insert($table, $values)  往指定表里插入一条记录
    + update($table, $values, $where)  更新指定表里符合条件的记录
    + delete($table, $where)  删除指定表里符合条件的记录
    + hasError()  判断是否出错
    + getError()  返回错误描述信息
    + getLogs()  返回所有查询执行记录及消耗时间、成败、影响行数
3. private 方法
    - connect()  检查/连接数据库
    - filterVars($vars)  过滤并组装查询参数
    - fetchArgs($args)  过滤并替换查询语句中的占位参数
    - fetchResult($type = MYSQLI_ASSOC, $singleRow = false)  将查询结果集取出放入数组返回
    - fetchError($errno = null, $error = null)  获取数据库错误信息


#### 使用说明

1.  简单示例如下：
```
//引入类库
require '../SpringMySQLi.php';

//初始化类库
$dbh = new SpringMySQLi($dbHost, $dbUser, $dbUpwd, $dbName);

//编写SQL并设置查询参数
$sql = 'SELECT * FROM member WHERE mid=#1';
$mid = 3;
$arr = $dbh->selectRow($sql, $mid); //sql执行时 #1 会被自动替换为安全过滤后的 $mid 值
print_r($arr);

//SQL中参数个数不限，但需要序号与传入位置一致
$sql = 'SELECT * FROM member WHERE username LIKE \'#1%\' AND status=#2';
$kw  = 'W';
$arr = $dbh->select($sql, $kw, '1');
print_r($arr);

//往指定表中插入新记录
$arr = array(
    'username'  => 'Taylor',
    'login_pwd' => md5('111111'),
    'email'     => 'taylor@test.com',
    'status'    => 1,
);
$mid = $dbh->insert('member', $arr);  //插入操作会返回新记录的ID
echo 'inserted new member: ', $mid;

//可以得到所有执行过的SQL信息（消耗时间、是否成功、影响数据行数）
$arr = $dbh->getLogs();
print_r($arr);

```

2.  可参考代码库中 **demo/test.php** 文件中的代码


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 感谢

1.  感谢每个来到这里的**码匠**
2.  感恩送出 Star 的你
3.  祝福 PHP 不死
